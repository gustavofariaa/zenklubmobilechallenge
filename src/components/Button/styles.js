import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  ${props => `
    background-color: ${props.theme.color.white};
    justify-content: center;
    align-items: center;
    margin: ${props.theme.spacing.spacing4};
    padding: ${props.theme.spacing.spacing4};
    border-radius: ${props.theme.spacing.spacing8};
  `}
`;

export const Text = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.normal};
    color: ${props.theme.color.primary};
    font-weight: ${props.theme.font.weight.bold};
    text-transform: uppercase;
  `}
`;
