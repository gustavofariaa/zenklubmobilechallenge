# Zenklub Mobile Challenge

## Design

Design on [Figma](https://www.figma.com/file/pvtttv24npyvTrkwobpgJD/Zenklub-Mobile-Challenge?node-id=0%3A1)

## Libraries

- [@react-navigation](https://github.com/react-navigation)
- [@miblanchard/react-native-slider](https://github.com/miblanchard/react-native-slider)
- [axios](https://github.com/axios/axios)

## Api

- [nextjs](https://nextjs.org/)

```
GET https://zenklub-challenge-api.vercel.app/api/feeling
```

### Response

```
[
  {
    id: Number,
    emoji: String,
    text: String,
    size: String // ["small", "medium", "large"]
  },
  ...
]
```

## How to execute

```
git clone https://gustavofariaa@bitbucket.org/gustavofariaa/zenklubmobilechallenge.git
cd .\zenklubmobilechallenge
npx react-native start
npx react-native run-android
```
