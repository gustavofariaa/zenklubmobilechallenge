import React from 'react';
import {Dimensions, StatusBar, FlatList, StyleSheet} from 'react-native';

import {Background} from './styles';
import {theme} from '../../styles/theme';

import Emoji from '../../components/Emoji';

export default ({route, navigation}) => {
  const {list} = route.params;

  const {width} = Dimensions.get('window');
  const columns = Math.trunc(width / 96);

  const navigate = item => {
    navigation.navigate('Feeling', {item});
  };

  return (
    <Background>
      <StatusBar
        backgroundColor={theme.color.primary}
        barStyle={'light-content'}
      />
      <FlatList
        data={list}
        columnWrapperStyle={styles.columnWrapperStyle}
        numColumns={columns}
        renderItem={({item}) => (
          <Emoji item={item} onPress={() => navigate(item)} />
        )}
        keyExtractor={item => item.id}
        nestedScrollEnabled
      />
    </Background>
  );
};

const styles = StyleSheet.create({
  columnWrapperStyle: {
    justifyContent: 'space-evenly',
  },
});
