import React, {useState} from 'react';
import {StatusBar} from 'react-native';

import {theme} from '../../styles/theme';

import {
  Background,
  TitleWrapper,
  TitleEmoji,
  Title,
  TitleFeeling,
  SubTitle,
  Body,
  Container,
} from './styles';

import Button from '../../components/Button';
import Slider from '../../components/Slider';

export default ({route, navigation}) => {
  const {item} = route.params;

  const intensityValues = {
    0: 'Slightly',
    25: 'A little',
    50: 'Fairly',
    75: 'Very',
    100: 'Extremely',
  };

  const [intensity, setIntensity] = useState(0);

  return (
    <Background>
      <StatusBar
        backgroundColor={theme.color.white}
        barStyle={'dark-content'}
      />
      <TitleWrapper>
        <TitleEmoji>{item.emoji}</TitleEmoji>
        <Title>{"I'm feeling"}</Title>
        <TitleFeeling>{`${intensityValues[intensity]} ${item.text}`}</TitleFeeling>
        <SubTitle>{'Choose the intensity of your feeling'}</SubTitle>
      </TitleWrapper>

      <Body>
        <Slider
          intensityValues={intensityValues}
          intensity={intensity}
          setIntensity={setIntensity}
        />
        <Container>
          <Button text={'Next'} onPress={() => navigation.goBack()} />
        </Container>
      </Body>
    </Background>
  );
};
