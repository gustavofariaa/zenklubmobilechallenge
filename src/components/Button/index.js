import React from 'react';

import {Container, Text} from './styles';

export default ({text, ...props}) => {
  return (
    <Container {...props}>
      <Text>{text}</Text>
    </Container>
  );
};
