import styled from 'styled-components/native';

export const SliderValueWrapper = styled.View`
  ${props => `
    justify-content: space-between;
    flex-direction: row;
    align-items: flex-end;
    margin-top: ${props.theme.spacing.spacing12};
  `}
`;

export const SliderValue = styled.Text`
  ${props => `
    width: ${props.width};
    font-weight: ${props.theme.font.weight.bold};
    text-align: center;
    ${
      props.selected
        ? `
        font-size: ${props.theme.font.size.large};
        color: ${props.theme.color.primary};
        padding-bottom: ${props.theme.spacing.spacing9};
      `
        : `
        font-size: ${props.theme.font.size.small};
        color: ${props.theme.color.lightGray};
        padding-bottom: ${props.theme.spacing.spacing1};
      `
    }
  `}
`;

export const SliderTextWrapper = styled.View`
  ${props => `
    justify-content: space-between;
    flex-direction: row;
    align-items: flex-start;
    background-color: ${props.theme.color.primary};
  `}
`;

export const SliderText = styled.Text`
  ${props => `
    width: ${props.width};
    color: ${props.theme.color.white};
    font-size: ${props.theme.font.size.small};
    text-align: center;
    margin-top: ${
      props.selected
        ? props.theme.spacing.spacing2
        : `-${props.theme.spacing.spacing9}`
    };
  `}
`;
