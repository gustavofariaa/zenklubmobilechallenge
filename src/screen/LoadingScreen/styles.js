import styled from 'styled-components/native';

export const Background = styled.View`
  ${props => `
    flex: 1;
    background-color: ${props.theme.color.primary};
    justify-content: center;
    align-items: center;
  `}
`;

export const Text = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.medium};
    color: ${props.theme.color.white};
    font-weight: ${props.theme.font.weight.bold};
  `}
`;
