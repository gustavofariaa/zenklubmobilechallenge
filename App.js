import React from 'react';
import {LogBox, Text} from 'react-native';
import {ThemeProvider} from 'styled-components';

import {theme} from './src/styles/theme';

import Router from './src/router';

LogBox.ignoreAllLogs();

export default () => {
  return (
    <ThemeProvider theme={theme}>
      <Router theme={theme} />
    </ThemeProvider>
  );
};

if (Text.defaultProps == null) {
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
}
