import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from '../screen/HomeScreen';
import FeelingScreen from '../screen/FeelingScreen';
import LoadingScreen from '../screen/LoadingScreen';

const Stack = createNativeStackNavigator();

export default ({theme}) => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShadowVisible: false,
        }}>
        <Stack.Screen
          name="Loading"
          component={LoadingScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'How are you feeling?',
            headerTintColor: theme.color.white,
            headerTitleStyle: {
              fontSize: theme.font.size.medium,
              fontWeight: theme.font.weight.bold,
            },
            headerStyle: {backgroundColor: theme.color.primary},
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="Feeling"
          component={FeelingScreen}
          options={{
            title: null,
            headerStyle: {backgroundColor: theme.color.white},
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
