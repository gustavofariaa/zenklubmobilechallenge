import React from 'react';
import {theme} from '../../styles/theme';

import {Wrapper, Container, Icon, Text} from './styles';

export default ({item, onPress = () => {}}) => {
  return (
    <Wrapper onPress={onPress}>
      <Container size={theme.emojiSize[item.size]}>
        <Icon>{item.emoji}</Icon>
        <Text>{item.text}</Text>
      </Container>
    </Wrapper>
  );
};
