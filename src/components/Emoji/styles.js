import styled from 'styled-components/native';

export const Wrapper = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export const Container = styled.View`
  ${props => `
    width: ${props.size};
    height: ${props.size};
    background-color: ${props.theme.color.whiteTransparency};
    border-radius: ${props.size};
    justify-content: center;
    align-items: center;
  `}
`;

export const Icon = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.large};
    margin-bottom: ${props.theme.spacing.spacing1};
  `}
`;

export const Text = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.small};
  `}
`;
