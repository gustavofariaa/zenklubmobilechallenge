import axios from 'axios';

const api = axios.create({
  baseURL: 'https://zenklub-challenge-api.vercel.app/api',
});

export default api;
