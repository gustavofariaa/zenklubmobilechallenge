import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {Slider} from '@miblanchard/react-native-slider';

import {theme} from '../../styles/theme';

import {
  SliderValueWrapper,
  SliderValue,
  SliderTextWrapper,
  SliderText,
} from './styles';

export default ({intensityValues, intensity, setIntensity = () => {}}) => {
  const {width} = Dimensions.get('window');
  const sliderValueWidth = width / 6;

  return (
    <>
      <SliderValueWrapper>
        <SliderValue width={sliderValueWidth} selected={intensity === 0}>
          {'0%'}
        </SliderValue>
        <SliderValue width={sliderValueWidth} selected={intensity === 25}>
          {'25%'}
        </SliderValue>
        <SliderValue width={sliderValueWidth} selected={intensity === 50}>
          {'50%'}
        </SliderValue>
        <SliderValue width={sliderValueWidth} selected={intensity === 75}>
          {'75%'}
        </SliderValue>
        <SliderValue width={sliderValueWidth} selected={intensity === 100}>
          {'100%'}
        </SliderValue>
      </SliderValueWrapper>
      <Slider
        thumbTintColor={theme.color.white}
        containerStyle={styles.containerStyle}
        thumbStyle={styles.thumbStyle}
        trackStyle={styles.trackStyle}
        maximumTrackTintColor={theme.color.transparent}
        minimumTrackTintColor={theme.color.transparent}
        value={intensity}
        step={25}
        maximumValue={100}
        onValueChange={value => setIntensity(value?.[0])}
      />
      <SliderTextWrapper>
        <SliderText width={sliderValueWidth} selected={intensity === 0}>
          {'Slightly'}
        </SliderText>
        <SliderText width={sliderValueWidth} selected={intensity === 25}>
          {'A little'}
        </SliderText>
        <SliderText width={sliderValueWidth} selected={intensity === 50}>
          {'Fairly'}
        </SliderText>
        <SliderText width={sliderValueWidth} selected={intensity === 75}>
          {'Very'}
        </SliderText>
        <SliderText width={sliderValueWidth} selected={intensity === 100}>
          {'Extremely'}
        </SliderText>
      </SliderTextWrapper>
    </>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: theme.color.primary,
    height: 40,
    justifyContent: 'flex-end',
  },
  thumbStyle: {
    width: 72,
    height: 72,
    borderRadius: 72,
    borderColor: theme.color.primary,
    borderWidth: 12,
  },
  trackStyle: {
    backgroundColor: 'transparent',
  },
});
