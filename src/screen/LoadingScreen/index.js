import React, {useState, useEffect} from 'react';

import api from '../../api';

import {Background, Text} from './styles';

export default ({navigation}) => {
  const [list, setList] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        const {data} = await api.get('/feeling');
        setList(data);
      } catch {
        console.error('error');
      }
    })();
  }, []);

  useEffect(() => {
    if (list?.length > 0) {
      navigation.replace('Home', {list});
    }
  }, [list, navigation]);

  return (
    <Background>
      <Text>Carregando...</Text>
    </Background>
  );
};
