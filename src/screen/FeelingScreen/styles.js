import styled from 'styled-components/native';

export const Background = styled.View`
  ${props => `
    flex: 1;
    background-color: ${props.theme.color.white};
  `}
`;

export const TitleWrapper = styled.View`
  ${props => `
    justify-content: center;
    align-items: center;
    padding: ${props.theme.spacing.spacing4};
  `}
`;

export const TitleEmoji = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.xxLarge};
    margin-bottom: ${props.theme.spacing.spacing4};
  `}
`;

export const Title = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.large};
    color: ${props.theme.color.black};
    margin-bottom: ${props.theme.spacing.spacing1};
    font-weight: ${props.theme.font.weight.bold};
  `}
`;

export const TitleFeeling = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.large};
    color: ${props.theme.color.primary};
    margin-bottom: ${props.theme.spacing.spacing4};
    font-weight: ${props.theme.font.weight.bold};
  `}
`;

export const SubTitle = styled.Text`
  ${props => `
    font-size: ${props.theme.font.size.small};
    color: ${props.theme.color.darkGray};
  `}
`;

export const Body = styled.View`
  flex: 1;
`;

export const Container = styled.View`
  ${props => `
    flex: 1;
    background-color: ${props.theme.color.primary};
    justify-content: flex-end;
  `}
`;
