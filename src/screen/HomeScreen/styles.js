import styled from 'styled-components/native';

export const Background = styled.View`
  ${props => `
    flex: 1;
    background-color: ${props.theme.color.primary};
    padding-vertical: ${props.theme.spacing.spacing4};
  `}
`;
